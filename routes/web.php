
<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\LogoutController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RegisterController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::view('/register', 'register.index')->name('register.index');
Route::post('/register/store', [RegisterController::class, 'store'])->name('register.store');

Route::get('/', [\App\Http\Controllers\MainController::class, 'index'])->name('main.index');
Route::view('register', 'register.index')->name('register.index');
Route::view('/login', 'login.index')->name('login.index');
Route::get('/logout', LogoutController::class)->name('logout');
Route::post('/login/store', [LoginController::class, 'store'])->name('login.store');

Route::patch('/profile/update/{id}', [ProfileController::class, 'update'])->name('profile.update');

Route::get('/profile/{id}', [ProfileController::class, 'index'])->name('profile.index');

Route::prefix('katalog')
    ->name('catalog.')
    ->controller(\App\Http\Controllers\CatalogController::class)
    ->group(function () {
        Route::get('/', 'index')->name('catalog');
        Route::get('/show/{id}', 'show')->name('show');
    });

Route::prefix('kontakts')
    ->name('kontakts.')
    ->controller(\App\Http\Controllers\ContactController::class)
    ->group(function () {
        Route::get('/', 'index')->name('kontakt');
        Route::post('/store', 'store')->name('store');
    });

Route::prefix('korzina')
    ->name('korzina.')
    ->group( function () {
    Route::get('/', [\App\Http\Controllers\BusketController::class, 'index'])->name('korzina');
    Route::post('/add-order', [\App\Http\Controllers\BusketController::class, 'addOrder'])->name('order');
    Route::post('/delete', [\App\Http\Controllers\BusketController::class, 'delete'])->name('delete');
    Route::post('/buy', [\App\Http\Controllers\BusketController::class, 'buyOrder'])->name('buy');
    Route::post('/buy-ball', [\App\Http\Controllers\BusketController::class, 'buyBall'])->name('buy-ball');
});
Route::view('/kab', 'kab.kab')->name('kab.kab');

Route::prefix('kab')
    ->name('kab.')
    ->group(function () {
        Route::get('/', [\App\Http\Controllers\KabController::class, 'index'])->name('kab');
        Route::post('/review', [\App\Http\Controllers\KabController::class, 'review'])->name('review');
        Route::delete('/delete/{id}', [\App\Http\Controllers\KabController::class, 'deleteAdress'])->name('delete');
        Route::post('/create', [\App\Http\Controllers\KabController::class, 'addAddress'])->name('create');
    });

Route::prefix('admin')
    ->name('admin.')
    ->group(function () {
        Route::get('/', [\App\Http\Controllers\Admin\DashboardController::class, 'index'])->name('index');

        Route::prefix('artist')
            ->name('artist.')
            ->controller(\App\Http\Controllers\Admin\ArtistController::class)
            ->group(function () {
                Route::get('/', 'index')->name('index');
                Route::get('/create', 'create')->name('create');
                Route::get('/edit/{id}', 'edit')->name('edit');
                Route::post('/store', 'store')->name('store');
                Route::patch('/update/{id}', 'update')->name('update');
                Route::delete('/destroy/{id}', 'delete')->name('destroy');
            });

        Route::prefix('genre')
            ->name('genre.')
            ->controller(\App\Http\Controllers\Admin\GenreController::class)
            ->group(function () {
                Route::get('/', 'index')->name('index');
                Route::get('/create', 'create')->name('create');
                Route::get('/edit/{id}', 'edit')->name('edit');
                Route::post('/store', 'store')->name('store');
                Route::patch('/update/{id}', 'update')->name('update');
                Route::delete('/destroy/{id}', 'delete')->name('destroy');
            });

        Route::prefix('album')
            ->name('album.')
            ->controller(\App\Http\Controllers\Admin\AlbumController::class)
            ->group(function () {
                Route::get('/', 'index')->name('index');
                Route::get('/create', 'create')->name('create');
                Route::get('/edit/{id}', 'edit')->name('edit');
                Route::post('/store', 'store')->name('store');
                Route::patch('/update/{id}', 'update')->name('update');
                Route::delete('/destroy/{id}', 'delete')->name('destroy');
            });

        Route::prefix('contact')
            ->name('contact.')
            ->controller(\App\Http\Controllers\Admin\ContactController::class)
            ->group(function () {
                Route::get('/', 'index')->name('index');
                Route::patch('/update', 'update')->name('update');
            });

        Route::prefix('user')
            ->name('user.')
            ->controller(\App\Http\Controllers\Admin\UserController::class)
            ->group(function () {
                Route::get('/', 'index')->name('index');
                Route::post('/block/{id}', 'block')->name('block');
                Route::delete('/destroy', 'delete')->name('destroy');
            });

        Route::prefix('feedback')
            ->name('feedback.')
            ->controller(\App\Http\Controllers\Admin\FeedbackController::class)
            ->group(function () {
                Route::get('/', 'index')->name('index');
                Route::get('/edit/{id}', 'edit')->name('edit');
                Route::delete('/delete/{id}', 'delete')->name('delete');
                Route::post('answer/{id}', 'answer')->name('answer');
            });

        Route::prefix('record')
            ->name('record.')
            ->controller(\App\Http\Controllers\Admin\RecordController::class)
            ->group(function () {
                Route::get('/', 'index')->name('index');
                Route::get('/create', 'create')->name('create');
                Route::get('/edit/{id}', 'edit')->name('edit');
                Route::post('/store', 'store')->name('store');
                Route::patch('/update/{id}', 'update')->name('update');
                Route::delete('/destroy/{id}', 'delete')->name('destroy');
            });

        Route::prefix('record-music')
            ->name('record-music.')
            ->controller(\App\Http\Controllers\Admin\RecordMusicController::class)
            ->group(function () {
                Route::get('/', 'index')->name('index');
                Route::get('/create', 'create')->name('create');
                Route::post('/store', 'store')->name('store');
                Route::get('/edit/{id}', 'edit')->name('edit');
                Route::delete('/delete/{id}', 'delete')->name('destroy');
            });

        Route::prefix('review')
            ->name('review.')
            ->controller(\App\Http\Controllers\Admin\ReviewController::class)
            ->group(function () {
                Route::get('/', 'index')->name('index');
                Route::delete('/delete/{id}', 'delete')->name('delete');
            });

        Route::prefix('order')
            ->name('order.')
            ->controller(\App\Http\Controllers\Admin\OrderController::class)
            ->group(function () {
                Route::get('/', 'index')->name('index');
                Route::get('/edit/{id}', 'edit')->name('edit');
                Route::patch('/update/{id}', 'update')->name('update');
            });

    });

Route::post('/kab/add', [\App\Http\Controllers\KabController::class, 'addAddress'])->name('kab.add');
