<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('record_music', function (Blueprint $table) {
            $table->id();
            $table->string('side_a')->nullable();
            $table->string('side_b')->nullable();
            $table->foreignIdFor(\App\Models\Record::class);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('record_music');
    }
};
