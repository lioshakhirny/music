<?php

namespace Database\Seeders;

use App\Enums\RoleEnum;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::query()->create([
            'name' => 'administrator',
            'email' => 'administrator@gmail.com',
            'password' => Hash::make('123456789'),
            'role_id' => RoleEnum::ADMIN->value
        ]);
    }
}
