<?php

namespace App\Http\Controllers;

use App\Models\Review;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index ()
    {
        return view('main.main', [
            'reviews' => Review::all()
        ]);
    }
}
