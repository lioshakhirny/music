<?php

namespace App\Http\Controllers;

use App\Enums\RoleEnum;
use App\Http\Requests\LoginRequest;

use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class LoginController extends Controller
{
    /**
     * @param LoginRequest $request
     * @return RedirectResponse
     */
    public function store (LoginRequest $request): RedirectResponse
    {
        if (!Cache::has('user-block-' . $request->email)) {
            if (Auth::attempt($request->only(['email', 'password']), $request->filled('remember'))) {

                if (\auth()->user()->role_id == RoleEnum::ADMIN->value) {
                    return redirect()->route('admin.index')->with('success', 'Вы успешно вошли в аккаунт!');
                } elseif (\auth()->user()->role_id == RoleEnum::USER->value) {
                    return redirect()->back()->with('success', 'Вы успешно вошли в аккаунт!');
                }
            }
        } else {
            return redirect()->back()->with('danger', 'Вы заблокированы на 24 часа');
        }

        return redirect()->back()->with('danger', 'Вы не зарегистрированы');
    }
}
