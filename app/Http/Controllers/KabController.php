<?php

namespace App\Http\Controllers;

use App\Models\Adress;
use App\Models\Order;
use App\Models\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KabController extends Controller
{
    public function index ()
    {
        return view('kab.kab', [
            'address' => Adress::query()->where('user_id', '=', Auth::id())->get(),
            'orders' => Order::query()->where('user_id', '=', Auth::id())->get()
        ]);
    }

    public function review (Request $request)
    {
        Review::query()->create([
            'email' => Auth::user()->email,
            'description' => $request->description
        ]);

        return redirect()->back();
    }

    public function deleteAdress(Adress $id)
    {
        $id->delete();

        return redirect()->back();
    }
    public function addAddress(Request $request)
    {
        $validated = $request->validate([
            'city' => 'required|string|max:255',
            'home' => 'required|string|max:255',
            'room' => 'required|string|max:255',
        ]);

        $address = Adress::create([
            'city' => $validated['city'],
            'home' => $validated['home'],
            'room' => $validated['room'],
            'user_id' => Auth::id(),
        ]);

        return redirect()->back();
    }
}
