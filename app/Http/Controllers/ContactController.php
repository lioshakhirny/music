<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Feedback;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index ()
    {
        return view('kontakts.kontakt', [
            'contact' => Contact::find(1)
        ]);
    }

    public function store (Request $request)
    {
        Feedback::query()->create([
            'name' => $request->name,
            'email' => $request->email,
            'description' => $request->description
        ]);

        return redirect()->route('kontakts.kontakt')->with('success', 'Спасибо за ваш вопрос мы обязательно вам ответим');
    }
}
