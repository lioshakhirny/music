<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Album;
use App\Models\Artist;
use App\Models\Genre;
use App\Models\Record;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class RecordController extends Controller
{
    public function index (): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view('admin.record.index', [
            'records' => Record::all()
        ]);
    }

    public function create (): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view('admin.record.create', [
            'albums' => Album::all(),
            'genres' => Genre::all(),
            'artists' => Artist::all()
        ]);
    }

    public function store (Request $request)
    {
        if ($request->file('image_url')) {
            $record = Record::query()->create([
                'name' => $request->name,
                'description' => $request->description,
                'image_url' => $request->file('image_url')->getClientOriginalName(),
                'price' => $request->price,
                'discount' => $request->discount,
                'genre_id' => $request->genre_id,
                'artist_id' => $request->artist_id,
                'album_id' => $request->album_id,
                'point' => $request->point
            ]);

            Storage::putFileAs('public/files/record/' . $record->id, $request->image_url, $request->image_url->getClientOriginalName());
        }

        return redirect()->route('admin.record.index');
    }

    public function edit (Record $id)
    {
        return view('admin.record.edit', [
            'record' => $id,
            'albums' => Album::all(),
            'genres' => Genre::all(),
            'artists' => Artist::all()
        ]);
    }

    public function update (Request $request, Record $id)
    {
        if($request->file('image_url')) {
            Storage::deleteDirectory('public/files/record/' . $id->id);
            Storage::putFileAs('public/files/record/' . $id->id, $request->image_url, $request->image_url->getClientOriginalName());
        }

        $id->update([
            'name' => $request->name,
            'description' => $request->description,
            'image_url' => $request->file('image_url')?->getClientOriginalName() ?? $id->image_url,
            'price' => $request->price,
            'discount' => $request->discount,
            'genre_id' => $request->genre_id,
            'artist_id' => $request->artist_id,
            'album_id' => $request->album_id,
            'point' => $request->point
        ]);

        return redirect()->route('admin.record.index');
    }

    public function delete (Record $id)
    {
        Storage::deleteDirectory('public/files/record/' . $id->id);
        $id->delete();

        return redirect()->route('admin.record.index');
    }
}
