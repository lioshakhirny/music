<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Genre\StoreRequest;
use App\Http\Requests\Genre\UpdateRequest;
use App\Models\Genre;
use Illuminate\Http\Request;

class GenreController extends Controller
{
    public function index ()
    {
        return view('admin.genre.index', [
            'genres' => Genre::all()
        ]);
    }

    public function create ()
    {
        return view('admin.genre.create');
    }

    public function store (StoreRequest $request)
    {
        Genre::query()->create([
            'title' => $request->title,
            'slug' => $request->slug
        ]);

        return redirect()->route('admin.genre.index')->with('success', 'Жанр успешно добавлен');
    }

    public function edit (Genre $id)
    {
        return view('admin.genre.edit', [
            'genre' => $id
        ]);
    }

    public function update (Genre $id, UpdateRequest $request)
    {
        $id->update([
            'title' => $request->title,
            'slug' => $request->slug
        ]);

        return redirect()->route('admin.genre.index')->with('success', 'Жанр успешно обновлен');
    }

    public function delete (Genre $id)
    {
        $id->delete();

        return redirect()->route('admin.genre.index')->with('success', 'Жанр успешно удален');
    }
}
