<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Genre\StoreRequest;
use App\Http\Requests\Genre\UpdateRequest;
use App\Models\Album;
use App\Models\Artist;
use App\Models\Genre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AlbumController extends Controller
{
    public function index ()
    {
        return view('admin.album.index', [
            'albums' => Album::all(),
        ]);
    }

    public function create ()
    {
        return view('admin.album.create', [
            'artists' => Artist::all()
        ]);
    }

    public function store (\App\Http\Requests\Album\StoreRequest $request)
    {
        Album::query()->create([
            'name' => $request->name,
            'artist_id' => $request->artist_id
        ]);

        return redirect()->route('admin.album.index')->with('success', 'Альбом успешно добавлен');
    }

    public function edit (Album $id)
    {
        return view('admin.album.edit', [
            'album' => $id,
            'artists' => Artist::all()
        ]);
    }

    public function update (Album $id, \App\Http\Requests\Album\UpdateRequest $request)
    {
        $id->update([
            'name' => $request->name,
            'artist_id' => $request->artist_id
        ]);

        return redirect()->route('admin.album.index')->with('success', 'Альбом успешно обновлен');
    }

    public function delete (Album $id)
    {
        $id->delete();

        return redirect()->route('admin.album.index')->with('success', 'Альбом успешно удален');
    }
}
