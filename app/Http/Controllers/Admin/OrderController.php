<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index ()
    {
        return view('admin.order.index', [
            'orders' => Order::all()
        ]);
    }

    public function edit (Order $id)
    {
        return view('admin.order.edit', [
            'order' => $id
        ]);
    }

    public function update (Request $request,  Order $id)
    {
        $id->update([
            'status' => $request->status
        ]);

        return redirect()->route('admin.order.index');
    }
}
