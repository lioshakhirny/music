<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Artist\StoreRequest;
use App\Http\Requests\Artist\UpdateRequest;
use App\Models\Artist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ArtistController extends Controller
{
    public function index (): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view('admin.artist.index', [
            'artists' => Artist::query()->get()
        ]);
    }

    public function create ()
    {
        return view('admin.artist.create');
    }

    public function store (StoreRequest $request)
    {
        if ($request->file('image_url')) {
            $artist = Artist::query()->create([
                'name' => $request->name,
                'description' => $request->description,
                'image_url' => $request->name . '.' . $request->image_url->getClientOriginalExtension()
            ]);

            $newFileName = $request->name . '.' . $request->image_url->getClientOriginalExtension();
            Storage::putFileAs('public/files/artist/' . $artist->id, $request->image_url, $newFileName);

            return redirect()->route('admin.artist.index')->with('success', 'Артист успешно создан');
        }

        return redirect()->route('admin.artist.index')->with('danger', 'Есть ошибки при создании артиста, проверьте правильность создания');
    }

    public function edit (Artist $id)
    {
        return view('admin.artist.edit', [
            'artist' => $id
        ]);
    }

    public function update (Artist $id, UpdateRequest $request)
    {
        if ($request->file('image_url')) {
            $id->update([
                'name' => $request->name,
                'description' => $request->description,
                'image_url' => $request->name . '.' . $request->image_url->getClientOriginalExtension()
            ]);

            Storage::deleteDirectory('public/files/artist/' . $id->id);

            $newFileName = $request->name . '.' . $request->image_url->getClientOriginalExtension();
            Storage::putFileAs('public/files/artist/' . $id->id, $request->image_url, $newFileName);

            return redirect()->route('admin.artist.index')->with('success', 'Артист успешно обновлен');
        }

        return redirect()->route('admin.artist.index')->with('danger', 'Есть ошибки проверьте правильность редактирование артиста');
    }

    public function delete (Artist $id)
    {
        Storage::deleteDirectory('public/files/artist/' . $id->id);
        $id->delete();

        return redirect()->route('admin.artist.index')->with('success', 'Артист успешно удален');
    }
}
