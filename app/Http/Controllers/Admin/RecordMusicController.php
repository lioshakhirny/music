<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Record;
use App\Models\RecordMusic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class RecordMusicController extends Controller
{
    public function index ()
    {
        return view('admin.record.music.index', [
            'recordMusics' => RecordMusic::all()
        ]);
    }

    public function create (): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view('admin.record.music.create', [
            'records' => Record::all()
        ]);
    }

    public function store (Request $request): \Illuminate\Http\RedirectResponse
    {
        foreach ($request->music_side_a as $sideA) {

           $recordMusic = RecordMusic::query()->create([
                'side_a' => $sideA->getclientOriginalName(),
                'side_b' => null,
                'record_id' => $request->record_id
            ]);

            Storage::putFileAs('public/files/music/sideA/' . $request->record_id, $sideA, $sideA->getClientOriginalName());
        }

        foreach ($request->music_side_b as $sideB)
        {
            $recordMusic = RecordMusic::query()->create([
                'side_b' => $sideB->getclientOriginalName(),
                'side_a' => null,
                'record_id' => $request->record_id
            ]);

            Storage::putFileAs('public/files/music/sideB/' . $request->record_id, $sideB, $sideB->getClientOriginalName());
        }

        return redirect()->route('admin.record-music.index')->with('success', 'Музыка успешно добавлена');
    }

    public function edit (RecordMusic $id)
    {
        return view('admin.record-music.edit', [
            'recordMusic' => $id
        ]);
    }

    public function update (RecordMusic $id, Request $request)
    {

    }

    public function delete (RecordMusic $id)
    {
        $id->delete();

        return redirect()->back()->with('success', 'Удаление успешно удалено');
    }
}
