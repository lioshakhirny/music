<?php

namespace App\Http\Controllers\Admin;

use App\Enums\RoleEnum;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class UserController extends Controller
{
    public function index ()
    {
        return view('admin.user.index', [
            'users' => User::query()->where('role_id', '=', RoleEnum::USER->value)->get()
        ]);
    }


    public function block (int $id)
    {
        $email = User::query()->find($id)->email;
        Cache::add('user-block-' . $email, $email, now()->addHours(24));

        return redirect()->route('admin.user.index');
    }

    public function delete (int $id)
    {
        User::query()->find($id)->delete();

        return redirect()->route('admin.user.index');
    }
}
