<?php

namespace App\Http\Controllers;

use App\Models\Record;
use App\Models\RecordMusic;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class CatalogController extends Controller
{
    public function index (Request $request)
    {
        $products = Record::query()
            ->when(!empty($request->search), fn(Builder $builder) => $builder->where('name', 'LIKE', '%' . $request->search . '%'))
            ->when(!empty($request->price) && $request->price === "max", fn(Builder $builder) => $builder->orderBy('price', 'desc'))
            ->when(!empty($request->price) && $request->price === "min", fn(Builder $builder) => $builder->orderBy('price', 'asc'))
            ->get();

        return view('catalog.catalog', [
            'records' => $products
        ]);
    }

    public function show (Record $id)
    {
        return view('cartochka.cartochka', [
            'record' => $id,
            'musics' => RecordMusic::query()->where('record_id', '=', $id->id)->get()
        ]);
    }
}
