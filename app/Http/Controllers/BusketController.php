<?php

namespace App\Http\Controllers;

use App\Enums\StatusEnum;
use App\Models\Adress;
use App\Models\Order;
use App\Models\Record;
use App\Models\UserPoint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class BusketController extends Controller
{
    public function index ()
    {
        return view('korzina.korzina', [
            'records' => Session::get('records'),
            'adresses' => Adress::query()->where('user_id', '=', auth()->user()->id)->get()
        ]);
    }

    public function addOrder (Request $request)
    {
        if (auth()->check()) {
            Session::start();
            $carts = Session::get('records');
            $product = Record::query()->find($request->record_id);
            if ($product->count() != 0) {
                if (isset($carts[$product->id])) {
                    $image = Storage::allFiles('public/files/record/' . $product->id);
                    $url = \Illuminate\Support\Facades\Storage::url($image[0]);
                    $count = $carts[$product->id]['count'] + 1;
                    $carts[$product->id] = [
                        'record_id' => $product->id,
                        'name' => $product->name,
                        'user_id' => auth()->user()->id,
                        'total_price' => $request->total_price * $count,
                        'count' => $count,
                        'url' => $url,
                        'point' => $product->point * $count
                    ];
                } else {
                    $image = Storage::allFiles('public/files/record/' . $product->id);
                    $url = \Illuminate\Support\Facades\Storage::url($image[0]);
                    $carts[$product->id] = [
                        'name' => $product->name,
                        'record_id' => $product->id,
                        'user_id' => auth()->user()->id,
                        'total_price' => $request->total_price,
                        'count' => 1,
                        'url' => $url,
                        'point' => $product->point
                    ];
                }
                Session::put('records', $carts);
                Session::save();

                return redirect()->route('korzina.korzina');
            }
        }

        return redirect()->route('catalog.catalog');
    }

    public function delete (Request $request)
    {
        Session::forget('records.' . $request->record_id);

        return redirect()->back();
    }

    public function buyOrder (Request $request)
    {
        $records = Session::get('records');

        foreach ($records as  $record) {

            Order::query()->create([
                'record_id' => $record['record_id'],
                'count' => $record['count'],
                'total_price' => $record['total_price'],
                'user_id' => auth()->user()->id,
                'address' => $request->address,
                'status' => StatusEnum::NEW->value
            ]);

            if (!empty($record['point'])) {
                $userPoint = UserPoint::query()
                    ->where('user_id', '=', Auth::id())
                    ->first();

                if (!empty($userPoint)) {
                    $userPoint->update([
                        'point' => $record['point'] + $userPoint->point
                    ]);
                } else {
                    UserPoint::query()->create([
                        'user_id' => Auth::id(),
                        'point' => $record['point']
                    ]);
                }
            }
        }

        Session::forget('records');

        return redirect()->back()->with('success', 'Спасибо за оформление заказа');
    }

    public function buyBall (Request $request)
    {

        $userPoint = UserPoint::query()->where('user_id', '=', Auth::id())->value('point') ?? null;

        if(!empty($userPoint) && $userPoint >= $request->ball) {
            UserPoint::query()->where('user_id', '=', Auth::id())->update([
               'point' => $userPoint - $request->ball
            ]);

            $records = Session::get('records');

            foreach ($records as  $record) {

                Order::query()->create([
                    'record_id' => $record['record_id'],
                    'count' => $record['count'],
                    'total_price' => $record['total_price'],
                    'user_id' => auth()->user()->id,
                    'address' => $request->address,
                    'status' => StatusEnum::NEW->value
                ]);
            }
            Session::forget('records');

            return redirect()->back()->with('success', 'Оплата за баллы прошла успешна');
        }

        return  redirect()->back()->with('danger', 'Оплата за баллы с ошибкой не хватает баллов');
    }

}
