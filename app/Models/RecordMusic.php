<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RecordMusic extends Model
{
    use HasFactory;

    protected $fillable = [
        'side_a',
        'side_b',
        'record_id'
    ];
}
