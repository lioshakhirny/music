<?php

namespace App\Enums;

enum StatusEnum: int
{
    case NEW = 1;
    case DELIVERY = 2;

    public function lable()
    {
        return match ($this) {
            self::NEW => 'В пути',
            self::DELIVERY => 'Доставлен'
        };
    }
}
