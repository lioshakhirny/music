<section class="record">
    <h1 class="record__title">Отзывыо компании</h1>
    <div class="record__container container">
        <div class="reviews-wrapper">
            <button class="prev-btn">&#10094;</button>
            <div class="reviews-container">
                @foreach($reviews as $review)
                    <div class="review-item">
                        <div class="review-content">
                            <div class="review-author">{{$review->email}}</div>
                            <div class="review-date">{{$review->created_at}}</div>
                            <p class="review-text">{{ $review->description }}</p>
                        </div>
                    </div>
                @endforeach
            </div>
            <button class="next-btn">&#10095;</button>
        </div>
    </div>
</section>
