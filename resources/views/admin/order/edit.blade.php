@extends('layouts.admin.main')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header pb-0">
                            <h4>Редактирование  заказа</h4>
                        </div>
                        <form method="post" action="{{route('admin.order.update', ['id' => $order->id])}}" enctype="multipart/form-data" class="form theme-form">
                            @method('PATCH')
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label class="form-label" for="exampleFormControlInput1"> Статус заказа</label>
                                            <select name="status">
                                                @foreach(\App\Enums\StatusEnum::cases() as $status)
                                                <option value="{{$status->value}}">{{$status->lable()}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-end">
                                <button class="btn btn-primary" type="submit">Редактировать  Заказ</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
