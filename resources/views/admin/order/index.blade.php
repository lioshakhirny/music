@extends('layouts.admin.main')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-title">
                <div class="row">
                    <div class="col-sm-6">
                        <h3>Заказы</h3>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.index') }}"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item">Заказы</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid basic_table">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Список заказов</h4>
                        </div>
                        <div class="table-responsive theme-scrollbar">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th scope="col">Id</th>
                                    <th scope="col">Пластинка</th>
                                    <th scope="col">Пользователь</th>
                                    <th scope="col">Цена</th>
                                    <th scope="col">Статус</th>
                                    <th scope="col">Адрес</th>
                                    <th scope="col">Действия</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $order)
                                    <tr>
                                        <th scope="row">{{$order->id}}</th>
                                        <td class="">{{ \App\Models\Record::find($order->record_id)->name}}</td>
                                        <td class="">{{\Illuminate\Support\Facades\Auth::user()->email}}</td>
                                        <td class="">{{$order->total_price}}</td>
                                        <td class="">{{\App\Enums\StatusEnum::tryFrom($order->status)->lable()}}</td>
                                        <td class="">{{$order->address}}</td>
                                        <td>
                                            <div class="row">
                                                <a href="{{route('admin.order.edit', ['id' => $order->id])}}">Редактировать</a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection
