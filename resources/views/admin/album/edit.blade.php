@extends('layouts.admin.main')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header pb-0">
                            <h4>Редактирование альбома</h4>
                        </div>
                        <form method="post" action="{{route('admin.album.update', ['id' => $album->id])}}" enctype="multipart/form-data" class="form theme-form">
                            @method('PATCH')
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label class="form-label" for="exampleFormControlInput1">Название</label>
                                            <input class="form-control" id="exampleFormControlInput1" name="name" value="{{$album->name}}" type="text" placeholder="Тимати">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label class="form-label" for="exampleFormControlInput1">Артист</label>
                                            <select class="form-control" name="artist_id">
                                                @foreach($artists as $artist)
                                                    <option value="{{$artist->id}}">{{$artist->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-end">
                                <button class="btn btn-primary" type="submit">Редактировать альбом</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
