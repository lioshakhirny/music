@extends('layouts.admin.main')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-title">
                <div class="row">
                    <div class="col-sm-6">
                        <h3>Обратная связь</h3>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('admin.index')}}"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item">Обратная связь</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid basic_table">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Список</h4>
                        </div>
                        <div class="table-responsive theme-scrollbar">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th scope="col">Id</th>
                                    <th scope="col">Имя</th>
                                    <th scope="col">Почта</th>
                                    <th scope="col">Действия</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($feedbacks as $feedback)
                                    <tr>
                                        <th scope="row">{{$feedback->id}}</th>
                                        <td>{{$feedback->name}}</td>
                                        <td>{{$feedback->email}}</td>
                                        <td>
                                            <a href="{{route('admin.feedback.edit', ['id' => $feedback->id])}}">Ответить</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
