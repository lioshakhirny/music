@extends('layouts.admin.main')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-title">
                <div class="row">
                    <div class="col-sm-6">
                        <h3>Музыка</h3>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.index') }}"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item">Музыка</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid basic_table">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Список музыки</h4>
                            <br>
                            <a href="{{route('admin.record-music.create')}}" class="btn btn-primary">Добавить музыку</a>
                        </div>
                        <div class="table-responsive theme-scrollbar">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th scope="col">Id</th>
                                    <th scope="col">Сторона А</th>
                                    <th scope="col">Сторона В</th>
                                    <th scope="col">Пластинка</th>
                                    <th scope="col">Действия</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($recordMusics as $recordMusic)
                                    <tr>
                                        <th scope="row">{{$recordMusic->id}}</th>
                                        <td class="">{{$recordMusic->side_a}}</td>
                                        <td class="">{{$recordMusic->side_b}}</td>
                                        <td class="">{{ \App\Models\Record::find($recordMusic->record_id)->name }}</td>
                                        <td>
                                            <div class="row">
                                                <form
                                                    action="{{route('admin.record-music.destroy', ['id' => $recordMusic->id])}}"
                                                    method="post">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button type="submit" class="btn btn-primary">Удалить</button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
