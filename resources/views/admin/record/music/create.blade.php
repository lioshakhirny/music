@extends('layouts.admin.main')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header pb-0">
                            <h4>Создание музыки</h4>
                        </div>
                        <form method="post" action="{{route('admin.record-music.store')}}" enctype="multipart/form-data" class="form theme-form">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label class="form-label" for="exampleInputPassword4">Музыка сторона А</label>
                                            <input class="form-control" multiple name="music_side_a[]" id="exampleInputPassword4" type="file">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label class="form-label" for="exampleInputPassword4">Музыка сторона B</label>
                                            <input class="form-control" multiple name="music_side_b[]" id="exampleInputPassword4" type="file">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label class="form-label" for="exampleFormControlInput1">Пластинка</label>
                                            <select class="form-control" name="record_id">
                                                @foreach($records as $record)
                                                    <option value="{{$record->id}}">{{$record->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-end">
                                <button class="btn btn-primary" type="submit">Добавить музыки</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
