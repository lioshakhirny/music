@extends('layouts.admin.main')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header pb-0">
                            <h4>Редактирование жанра</h4>
                        </div>
                        <form method="post" action="{{route('admin.genre.update', ['id' => $genre->id])}}" enctype="multipart/form-data" class="form theme-form">
                            @method('PATCH')
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label class="form-label" for="exampleFormControlInput1"> Название</label>
                                            <input class="form-control" id="exampleFormControlInput1" name="title" value="{{$genre->title}}" type="text" placeholder="Тимати">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label class="form-label" for="exampleFormControlInput1">Слаг</label>
                                            <input class="form-control" id="exampleFormControlInput1" value="{{$genre->slug}}" name="slug" type="text" placeholder="Rock">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-end">
                                <button class="btn btn-primary" type="submit">Редактировать жанр</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
