<div class="obz-cont">
    <div class="container-korz">
        <div class="container-korz">
            @if(!empty($records))
                @foreach($records as $record)
                    <div class="cart-item">
                        <img src="{{$record['url']}}" width="250" alt="Product Image">
                        <h3 class="product-name">{{ $record['name'] }}</h3>
                        <div class="quantity">
                            <button class="decrease">-</button>
                            <span class="quantity-number">{{$record['count']}}</span>
                            <button class="increase">+</button>
                        </div>
                        <p class="price">{{ $record['total_price'] }} BUY</p>
                        <form action="{{route('korzina.delete') }}" method="post">
                            @csrf
                            <input type="hidden" name="record_id" value="{{ $record['record_id'] }}">
                            <button type="submit" class="remove">
                                <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24">
                                    <path fill="black"
                                          d="M7 21q-.825 0-1.412-.587T5 19V6H4V4h5V3h6v1h5v2h-1v13q0 .825-.587 1.413T17 21zM17 6H7v13h10zM9 17h2V8H9zm4 0h2V8h-2zM7 6v13z"/>
                                </svg>
                            </button>
                        </form>
                        @endforeach
                        <div id="order-modal" class="modal">
                            <div class="modal-content">
                                <form action="{{route('korzina.buy')}}" method="post">
                                    @csrf
                                    <span class="close">&times;</span>
                                    <h2>Оформление заказа</h2>
                                    <label for="record-title">Название пластинок:</label>
                                    @php
                                        $names = implode(', ', array_map(function ($value) {
                                             return $value['name'];
                                         }, $records));
                                    @endphp
                                    <span>{{ $names  }}</span>
                                    <label for="delivery-address">Адрес доставки:</label>
                                    <select name="address">
                                        @foreach($adresses as $adress )
                                            <option
                                                value="{{$adress->city . ' ' . $adress->home . ' ' . $adress->room}}">{{$adress->city . ' ' . $adress->home . ' ' . $adress->room}}</option>
                                        @endforeach
                                    </select>
                                    <label for="payment-method">Оплата:</label>
                                    <select id="payment-method" name="payment-method">
                                        <option value="cash">Наличными</option>
                                    </select>

                                    <label for="delivery-address">Начисление баллок за покупку:</label>
                                    <input type="text" class="bonus" name="point" disabled  placeholder="{{array_sum(array_column($records, 'point'))}}">
                                    <label for="total-amount">Стоимость общая:</label>
                                    <span>{{array_sum(array_column($records, 'total_price'))}}</span>

                                    <button type="submit" id="confirm-order">Оформить заказ</button>
                                </form>
                            </div>
                        </div>
                        <button class="oform-zakaz">Оформить заказ</button>
                        <form action="{{ route('korzina.buy-ball') }}" method="post">
                            @csrf
                            <input type="hidden" name="ball" value="{{array_sum(array_column($records, 'total_price'))}}">
                            <label>Выберите адрес от покупок баллов:</label>
                            <select name="address">
                                @foreach($adresses as $adress )
                                    <option
                                        value="{{$adress->city . ' ' . $adress->home . ' ' . $adress->room}}">{{$adress->city . ' ' . $adress->home . ' ' . $adress->room}}</option>
                                @endforeach
                            </select>
                            <button class="oform-zakaz" type="submit">Оплата баллами</button>
                        </form>
                        @else
                            <div class="block-gif">
                                <img src="{{ asset('images/lk.gif') }}" alt="">
                            </div>
                            <h3 class="vibor">Вы ничего не выбрали, если хотите что то выбрать <a
                                    href="{{ route('catalog.catalog') }}">нажмите
                                    сюда.</a></h3>
                        @endif
                    </div>
        </div>
    </div>
</div>

@if(!empty(\Illuminate\Support\Facades\Session::get('success')))
    <script>
        alert({{\Illuminate\Support\Facades\Session::get('success')}})
    </script>
@elseif(!empty(\Illuminate\Support\Facades\Session::get('danger')))
    <script>
        alert({{\Illuminate\Support\Facades\Session::get('danger')}})
    </script>
@endif
<script>
    document.addEventListener('DOMContentLoaded', () => {
        const modal = document.getElementById('order-modal');
        const openBtn = document.querySelector('.oform-zakaz');
        const closeBtn = document.querySelector('.close');
        const confirmOrderBtn = document.getElementById('confirm-order');

        openBtn.addEventListener('click', () => {
            modal.style.display = 'flex';
        });

        closeBtn.addEventListener('click', () => {
            modal.style.display = 'none';
        });

        window.addEventListener('click', (event) => {
            if (event.target == modal) {
                modal.style.display = 'none';
            }
        });

        confirmOrderBtn.addEventListener('click', () => {
            alert('Заказ оформлен!');
            modal.style.display = 'none';
        });
    });

</script>
