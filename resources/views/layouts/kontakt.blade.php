<div class="container-kontakt">
    <h1>Контакты</h1>
    <div class="contact-info">
        <p><strong>Наш адрес:</strong><br>
         {{$contact->address ?? ''}}
        </p>

        <p><strong>Телефон:</strong><br>
            {{$contact->phone ?? '+7 (XXX) XXX-XX-XX'}}</p>

        <p><strong>Электронная почта:</strong><br>
            {{$contact->email ?? 'info@vinylrecords.com'}}</p>

        <p><strong>Рабочие часы:</strong><br>
           {{$contact->work ?? ''}}
        </p>
    </div>
    <div class="contact-form">
        <h2>Свяжитесь с нами</h2>
        <form action="{{route('kontakts.store')}}" method="post">
            @csrf
            <input type="text" name="name" placeholder="Ваше имя" required><br>
            <input type="email" name="email" placeholder="Ваш Email" required><br>
            <textarea name="description" placeholder="Ваше сообщение" required></textarea><br>
            <button type="submit">Отправить</button>
        </form>
    </div>
    <div class="social-media">
        <h2>Социальные сети</h2>
        <a href="#">Facebook</a><br>
        <a href="#">Instagram</a><br>
        <a href="#">Twitter</a><br>
    </div>
</div>
