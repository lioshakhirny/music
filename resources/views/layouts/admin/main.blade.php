<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="tivo admin is super flexible, powerful, clean &amp; modern responsive bootstrap 5 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Tivo admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="../assets/images/favicon/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="../assets/images/favicon/favicon.png" type="image/x-icon">
    <title>Административная панель</title>
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../assets/css/vendors/font-awesome.css {{asset('assets/admin/css/vendors/font-awesome.css')}}">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/vendors/icofont.css')}}">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href=" {{asset('assets/admin/css/vendors/themify.css')}}">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/vendors/flag-icon.css')}}">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/vendors/feather-icon.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/vendors/scrollbar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/vendors/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/vendors/chartist.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/vendors/prism.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/vendors/vendors/vector-map.css')}}">
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/vendors/bootstrap.css')}}">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/style.css')}}">
    <link id="color" rel="stylesheet" href="{{asset('assets/admin/css/color-1.css')}}" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/responsive.css')}}">
</head>
<body onload="startTime()">
<div class="tap-top"><i data-feather="chevrons-up"></i></div>
<div class="loader-wrapper">
    <div class="dot"></div>
    <div class="dot"></div>
    <div class="dot"></div>
    <div class="dot"> </div>
    <div class="dot"></div>
</div>

<div class="page-wrapper compact-wrapper" id="pageWrapper">
    <div class="page-body-wrapper">
        @include('include.admin.header')
        @include('include.admin.sidebar')
        @yield('content')
        @include('include.admin.footer')
    </div>

</div>

<script src="{{asset('assets/admin/js/jquery-3.6.0.min.js')}}"></script>
<!-- Bootstrap js-->
<script src="{{asset('assets/admin/js/bootstrap/bootstrap.bundle.min.js')}}  "></script>
<!-- feather icon js-->
<script src="{{asset('assets/admin/js/icons/feather-icon/feather.min.js')}}"></script>
<script src="{{asset('assets/admin/js/icons/feather-icon/feather-icon.jss')}}"></script>
<!-- scrollbar js-->
<script src="{{asset('assets/admin/js/scrollbar/simplebar.js')}}"></script>
<script src="{{asset('assets/admin/js/scrollbar/custom.js')}}"></script>
<!-- Sidebar jquery-->
<script src="{{asset('assets/admin/js/config.js')}}"></script>
<script src="{{asset('assets/admin/js/sidebar-menu.js')}}"></script>
<script src="{{asset('assets/admin/js/chart/chartist/chartist.js')}}"></script>
<script src="{{asset('assets/admin/js/chart/chartist/chartist-plugin-tooltip.js')}}"></script>
<script src="{{asset('assets/admin/js/chart/apex-chart/apex-chart.js')}}"></script>
<script src="{{asset('assets/admin/js/chart/apex-chart/stock-prices.js')}}"></script>
<script src="{{asset('assets/admin/js/prism/prism.min.js')}}"></script>
<script src="{{asset('assets/admin/js/clipboard/clipboard.min.js')}}"></script>
<script src="{{asset('assets/admin/js/custom-card/custom-card.js')}}"></script>
<script src="{{asset('assets/admin/js/notify/bootstrap-notify.min.js')}}"></script>
<script src="{{asset('assets/admin/js/vector-map/jquery-jvectormap-2.0.2.min.js')}}"></script>
<script src="{{asset('assets/admin/js/vector-map/map/jquery-jvectormap-world-mill-en.js')}}"></script>
<script src="{{asset('assets/admin/js/vector-map/map/jquery-jvectormap-us-aea-en.jss')}}"></script>
<script src="{{asset('assets/admin/js/vector-map/map/jquery-jvectormap-uk-mill-en.js')}}"></script>
<script src="{{asset('assets/admin/js/vector-map/map/jquery-jvectormap-au-mill.js')}}"></script>
<script src="{{asset('assets/admin/js/vector-map/map/jquery-jvectormap-chicago-mill-en.js')}}"></script>
<script src="{{asset('assets/admin/js/vector-map/map/jquery-jvectormap-in-mill.js')}}"></script>
<script src="{{asset('assets/admin/js/vector-map/map/jquery-jvectormap-asia-mill.js')}}"></script>
<script src="{{asset('assets/admin/js/dashboard/default.js')}}"></script>
<script src="{{asset('assets/admin/js/notify/index.js s')}}"></script>
<script src="{{asset('assets/admin/js/typeahead/handlebars.js')}}"></script>
<script src="{{asset('assets/admin/js/typeahead/typeahead.bundle.js')}}"></script>
<script src="{{asset('assets/admin/js/typeahead/typeahead.custom.js')}}"></script>
<script src="{{asset('assets/admin/js/typeahead-search/handlebars.js ')}}"></script>
<script src="{{asset('assets/admin/js/typeahead-search/typeahead-custom.js')}}"></script>
<!-- Template js-->
<script src="{{asset('assets/admin/js/script.js')}}"></script>
<script src="{{asset('assets/admin/js/theme-customizer/customizer.js')}}">  </script>
</body>
</html>
