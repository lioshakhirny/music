<div class="container-filter">
    <div class="search-container">
        <form action="{{route('catalog.catalog')}}" method="get">
            @csrf
            <div style="display: flex">
                <input type="text" id="search" name="search" placeholder="Поиск..." style="margin-bottom: 20px">
                <div class="custom-select" style="margin-left: 20px">
                    <select name="price" id="genre">
                        <option value="">Фильтр по цене</option>
                        <option value="max">От большего</option>
                        <option value="min">От меньшего</option>
                    </select>
                </div>
            </div>
        <button type="submit">Найти</button>
        </form>
    </div>
</div>
