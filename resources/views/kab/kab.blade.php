<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('dist/css/app/app.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.css"/>
    <script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>
    <script src="{{ asset('dist/js/app/app.js') }}"></script>
    <title>Пластинка</title>
</head>
<body>
<div class="hero__block-wrapper">
    @include('layouts.header')
</div>
<main>
    <div class="container-personal-account">
        <h1 class="personal-account-title">Личный кабинет</h1>
        <div class="addresses-section">
            <h2 class="section-title">Адреса доставки</h2>
            <ul id="address-list" class="address-list">
                @foreach($address as $adress)
                    <li class="address-item">
                        <span class="address-text">{{ $adress->city . ' ' . $adress->home . ' ' . $adress->room }}</span>
                        <form action="{{ route('kab.delete', ['id' => $adress->id]) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="delete-address-btn btn btn-danger">Удалить</button>
                        </form>
                    </li>
                @endforeach
            </ul>
            <button id="add-address-btn" class="btn">Добавить адрес</button>
            <form id="add-address-form" action="{{ route('kab.create') }}" method="post" style="display:none;">
                @csrf
                <input type="text" name="city" placeholder="Город" required>
                <input type="text" name="home" placeholder="Дом" required>
                <input type="text" name="room" placeholder="Комната" required>
                <button type="submit" >Сохранить</button>
            </form>
        </div>

        <div class="orders-section">
            <h2 class="section-title">Мои заказы</h2>
            <ul id="order-list" class="order-list">
                @if(!empty($orders))
                    @foreach($orders as $order)
                        <li class="order-item">
                            <span class="order-text">Заказ № {{$order->id}} - Статус: {{\App\Enums\StatusEnum::tryFrom($order->status)->lable()}}</span>
                            <button class="view-order-btn btn">Понятно</button>
                        </li>
                    @endforeach
                @else
                    <p>Список пуст</p>
                @endif
            </ul>
        </div>

        <div class="reviews-section">
            <h2 class="section-title">Оставить отзыв</h2>
            <form id="review-form" action="{{route('kab.review')}}" method="post">
                @csrf
                <label for="review" class="form-label">Ваш отзыв:</label>
                <textarea id="review" class="form-input" name="description" rows="4" placeholder="Напишите ваш отзыв"></textarea>
                <button type="submit" class="btn">Отправить отзыв</button>
            </form>
        </div>

        <div class="bonus-section">
            <h2 class="section-title">Бонусные баллы</h2>
            <p>Ваши бонусные баллы: <span id="bonus-points">500</span></p>
        </div>
    </div>
</main>
@include('layouts.footer')
</body>
<script>
    document.addEventListener('DOMContentLoaded', () => {
        const profileForm = document.getElementById('profile-form');
        const editProfileBtn = document.getElementById('edit-profile-btn');
        const cancelEditProfileBtn = document.getElementById('cancel-edit-profile');
        const profileInfo = document.querySelector('.profile-info');
        const editProfileForm = document.querySelector('.edit-profile-form');

        const addressForm = document.getElementById('address-form');
        const addAddressBtn = document.getElementById('add-address-btn');
        const cancelAddAddressBtn = document.getElementById('cancel-add-address');
        const addressList = document.getElementById('address-list');
        const addAddressForm = document.querySelector('.add-address-form');

        document.getElementById('add-address-btn').addEventListener('click', function() {
            document.getElementById('add-address-form').style.display = 'block';
        });


        // Редактирование и удаление адреса
        addressList.addEventListener('click', (e) => {
            if (e.target.classList.contains('edit-address-btn')) {
                const li = e.target.closest('.address-item');
                document.getElementById('address').value = li.querySelector('.address-text').textContent;
                addAddressForm.style.display = 'block';
                editAddressIndex = Array.from(addressList.children).indexOf(li);
            } else if (e.target.classList.contains('delete-address-btn')) {
                e.target.closest('.address-item').remove();
            }
        });

        // Просмотр заказа
        orderList.addEventListener('click', (e) => {
            if (e.target.classList.contains('view-order-btn')) {
                const orderInfo = e.target.closest('.order-item').querySelector('.order-text').textContent;
                alert(`Информация о заказе: ${orderInfo}`);
            }
        });
    });

</script>
</html>
